# coding: utf-8

#    El Caballero Rampante

import pygame
from pygame.locals import *
import utils

class Prota(pygame.sprite.Sprite):

    def __init__(self,bas):
        pygame.sprite.Sprite.__init__(self) #call Sprite initializer
        self.image, self.rect = utils.load_image("prota.png", -1)
        self.pinchando = False
        self.bas = bas

    def update(self):
        pos = pygame.mouse.get_pos()
        self.rect.center = pos

    def pinchar(self):
        if not self.pinchando:
            self.bas.snd_pinchazo.play()
            self.pinchando = True
            pygame.time.set_timer(utils.RECUPERAR_PROTA, 1000)  #no pincharse más en un rato
