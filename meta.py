# coding: utf-8

#    El Caballero Rampante

import pygame, random
from pygame.locals import *
import utils,rampante

class Meta(pygame.sprite.Sprite):

    def __init__(self,bas):
        pygame.sprite.Sprite.__init__(self) #call Sprite initializer
        self.abierta, self.rect_abierta = utils.load_image("meta.png", -1)
        self.cerrada, self.rect_cerrada = utils.load_image("meta cerrada.png", -1)
        self.image = self.cerrada
        self.rect = self.rect_cerrada
        self.rect.topleft = 100 + random.random()*600, 100 + random.random()*400
        self.bas=bas

    def abrir(self):
        self.bas.metas.clear(self.bas.screen,self.bas.fondo)
        self.image = self.abierta
        topleft = self.rect.topleft
        self.rect = self.rect_abierta
        self.rect.topleft = topleft
        self.bas.snd_abrir.play()
        self.bas.puerta_abierta = True

    def cerrar(self):
        self.bas.metas.clear(self.bas.screen,self.bas.fondo)
        self.image = self.cerrada
        topleft = self.rect.topleft
        self.rect = self.rect_cerrada
        self.rect.topleft = topleft
        self.bas.snd_cerrar.play()
        self.bas.puerta_abierta = False
