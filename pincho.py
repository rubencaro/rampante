# coding: utf-8

#    El Caballero Rampante

import pygame, random
from pygame.locals import *
import utils,rampante

class Pincho(pygame.sprite.Sprite):

    def __init__(self,bas,seg=False):
        pygame.sprite.Sprite.__init__(self) #call Sprite initializer
        self.bas = bas
        self.image, self.rect = utils.load_image("pincho.png", -1)
        self.area = self.bas.screen.get_rect()
        self.seguimiento = seg
        self.margen = 40
        if self.seguimiento:
            self.rect.topleft = self.bas.meta.rect.topleft
        else:
            self.rect.topleft = 100 + random.random()*600, 100 + random.random()*400
        self.movex = 1 + random.random()*2
        self.movey = 1 + random.random()*2

    def update(self):
        newpos = self.rect.move((self.movex, self.movey))
        if self.seguimiento :
            if self.rect.left < self.bas.meta.rect.left - self.margen or \
                self.rect.right > self.bas.meta.rect.right + self.margen:
                self.movex = -self.movex
            if self.rect.top < self.bas.meta.rect.top - self.margen or \
                self.rect.bottom > self.bas.meta.rect.bottom + self.margen:
                self.movey = -self.movey
        else:
            if self.rect.left < self.area.left or \
                self.rect.right > self.area.right:
                self.movex = -self.movex
            if self.rect.top < self.area.top or \
                self.rect.bottom > self.area.bottom:
                self.movey = -self.movey
        newpos = self.rect.move((self.movex, self.movey))
        self.rect = newpos
