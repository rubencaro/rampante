#!/usr/bin/env python
# coding: utf-8

#    El Caballero Rampante

import pygame, sys, os
from pygame.locals import *
import prota,llave,pincho,utils,meta

if not pygame.font: print "Fuentes no disponibles"
if not pygame.mixer: print "Sonido no disponible"

class Rampante:

    version = "El Caballero Rampante v1.0"

    def __init__(self):
        pygame.init()

        self.pantalla_completa = False
        self.window = pygame.display.set_mode((800,600))

        pygame.display.set_caption(self.version)
        pygame.mouse.set_visible(0)

        self.screen = pygame.display.get_surface()

        (self.imagen_fondo,rect) = utils.load_image("fondo.png")

        (self.fondo,rect) = utils.load_image("fondo.png")

        self.rect_reloj = (0,0,0,0)    # tamaño de la pancarta de tiempo
        self.rect_nivel = (0,0,0,0) # tamaño de la pancarta de nivel

        self.snd_reloj_rapido = utils.load_sound('reloj rapido.wav')
        self.snd_reloj_lento = utils.load_sound('reloj lento.wav')
        self.snd_llave = utils.load_sound('llave.wav')
        self.snd_pinchazo = utils.load_sound('pinchazo.wav')
        self.snd_abrir = utils.load_sound('abrir.wav')
        self.snd_cerrar = utils.load_sound('cerrar.wav')
        self.snd_derrota = utils.load_sound('derrota.wav')
        self.snd_victoria = utils.load_sound('victoria.wav')
        self.snd_intro = utils.load_sound('intro.wav')
        self.snd_inicio = utils.load_sound('inicio.wav')
        self.snd_aplauso = utils.load_sound('aplauso.wav')

        self.fnt_reloj = os.path.join('fuentes', 'reloj.ttf')
        self.fnt_nivel = os.path.join('fuentes', 'mensajes.ttf')

        self.clock = pygame.time.Clock() #para controlar los FPS

        self.jugar = False
        self.esperando_creditos = False
        self.esperando_reinicio = False
        self.esperando_jugar = False
        self.tiempo = 0
        self.nivel_inicial = 1
        self.nivel = self.nivel_inicial
        self.maxllaves = 15
        self.maxllaves_nivel = 15
        self.puerta_abierta = False

        self.intro()
        self.bucle()

    def eventos(self):
        for event in pygame.event.get():
            if self.jugar and event.type == MOUSEBUTTONDOWN:
                if not self.puerta_abierta: #si la puerta está cerrada
                    colisiones = pygame.sprite.spritecollide(self.prota, self.llaves, False)
                    if len(colisiones) > 1:
                        self.snd_aplauso.play()
                    for llave in colisiones:
                        self.snd_llave.play() #coge la llave
                        llave.coger()
                    if len (self.llaves) == 0:  #si no quedan llaves se abre la puerta
                        self.meta.abrir()
                else:   #si está abierta
                    colisiones = pygame.sprite.spritecollide(self.prota, self.metas, False)
                    if len (colisiones) > 0:    #si toca la meta
                        self.inicio(True)

            elif self.jugar and event.type == utils.CUENTA_ATRAS:
                self.actualizar_cuenta_atras()
            elif self.jugar and event.type == utils.RECUPERAR_PROTA:
                self.prota.pinchando = False
                pygame.time.set_timer(utils.RECUPERAR_PROTA, 0)
            elif event.type == KEYDOWN and event.key == K_ESCAPE:
                if self.esperando_intro:
                    sys.exit(0)
                else:
                    self.snd_intro.stop()
                    self.intro()
            elif self.esperando_creditos and event.type == KEYDOWN and event.key == K_RETURN:
                self.creditos()
            elif self.esperando_reinicio and event.type == KEYDOWN and event.key == K_RETURN:
                self.snd_intro.fadeout(2000)
                self.inicio()
            elif self.esperando_intro and event.type == KEYDOWN and event.key == K_RETURN:
                self.snd_intro.fadeout(3000)
                self.inicio()
            elif self.esperando_intro and event.type == KEYDOWN and event.key == K_f:
                if self.pantalla_completa:
                    self.window = pygame.display.set_mode((800,600))
                    self.pantalla_completa = False
                else:
                    modos = pygame.display.list_modes()
                    if (800,600) in modos:  #si se puede a pantalla completa
                        self.window = pygame.display.set_mode((800,600), FULLSCREEN)
                        self.pantalla_completa = True
                    else:
                        self.window = pygame.display.set_mode((800,600))
                        self.pantalla_completa = False
                self.snd_intro.stop()
                self.intro()

            elif self.esperando_jugar and event.type == KEYDOWN and event.key == K_RETURN:
                self.snd_intro.fadeout(3000)
                self.snd_aplauso.fadeout(2000)
                self.empezar()
            elif event.type == QUIT:
                sys.exit(0)

    def actualizar_cuenta_atras(self):
        self.snd_reloj_rapido.play()
        if self.tiempo < 10:    #cambiar de color si son menos de 10 segs
            color = (255,0,0)
            self.snd_reloj_lento.play()
        else:
            color=(255,230,0)
        self.actualizar_reloj(color)
        if self.tiempo==0:   #se acaba
            pygame.time.set_timer(utils.CUENTA_ATRAS, 0)    #para el temporizador
            self.fin()
        self.tiempo-=1

    def actualizar_reloj(self,color):
        if self.rect_reloj is not (0,0,0,0):
            self.fondo.blit(self.imagen_fondo,(self.rect_reloj[0],self.rect_reloj[1]),self.rect_reloj)   # borrar
        surf = utils.font2surf(str(self.tiempo),96,color,self.fnt_reloj) # crear surf
        self.rect_reloj = (200,10,max(self.rect_reloj[2],surf.get_width()) ,max(self.rect_reloj[3],surf.get_height()) )  # actualizar tamaño
        self.fondo.blit(surf,(self.rect_reloj[0],self.rect_reloj[1])) # dibujar

    def actualizar_fondo(self):
        self.screen.blit(self.fondo,self.rect_reloj,self.rect_reloj)    #reloj
        self.screen.blit(self.fondo,self.rect_nivel,self.rect_nivel)    #nivel

    def bucle(self):
        while True:
            self.clock.tick(60)  #no más de 60fps
            self.eventos()
            if self.jugar:
                self.llaves.clear(self.screen,self.fondo)
                self.pinchos.clear(self.screen,self.fondo)
                self.protas.clear(self.screen,self.fondo)
                self.actualizar_fondo()
                self.metas.draw(self.screen)
                self.llaves.update()
                self.llaves.draw(self.screen)
                self.pinchos.update()
                self.pinchos.draw(self.screen)
                self.protas.update()
                self.protas.draw(self.screen)
                if not self.prota.pinchando:
                    self.comprueba_pinchos()
            pygame.display.flip()
            pygame.time.wait(10)    #dejar respirar al ordenador

    def txt_nivel(self,nombre):
        color=(0,200,0)
        surf = utils.font2surf(nombre,32,color,self.fnt_nivel) # crear surf
        self.rect_nivel = self.fondo.blit(surf,(350,10))

    def creditos(self):
        self.esperando_creditos = False
        color=(0,0,0)
        (auca,rect) = utils.load_image("pincho.png")
        (pyg,rect) = utils.load_image("pygame.gif")
        mensaje1 = "Meid in Espein"
        mensaje2 = self.version + " GPL 2013"
        fuente = os.path.join('fuentes', 'creditos.ttf')
        surf1 = utils.font2surf(mensaje1,24,color,fuente)
        surf2 = utils.font2surf(mensaje2,24,color,fuente)
        self.screen.blit(surf1,(230,520))
        self.screen.blit(pyg,(20,520))
        self.screen.blit(auca,(150,500))
        self.screen.blit(surf2,(230,545))
        self.snd_intro.play(-1)     #loop infinito
        self.esperando_reinicio = True

    def fin(self):
        self.jugar = False
        self.actualizar_reloj((255,0,0))
        self.actualizar_fondo()
        color=(255,0,0)
        mensaje1 = "El tiempo se ha acabado..."
        mensaje2 = "(Pulsa enter)"
        fuente = os.path.join('fuentes', 'mensajes.ttf')
        surf1 = utils.font2surf(mensaje1,36,color,fuente)
        surf2 = utils.font2surf(mensaje2,10,color,fuente)
        self.screen.blit(surf1,(70,200))
        self.screen.blit(surf2,(70,250))
        self.snd_derrota.play()
        self.esperando_creditos = True

    def intro(self):
        self.esperando_intro = True
        self.jugar = False
        self.esperando_creditos = False
        self.esperando_reinicio = False
        self.esperando_jugar = False
        self.nivel = self.nivel_inicial

        self.fondo.blit(self.imagen_fondo, (0,0))
        self.screen.blit(self.fondo, (0,0))
        pygame.display.flip()

        color0=(255,240,0)
        color=(250,0,150)
        mensaje0 = "El Caballero Rampante"
        mensaje1 = "Consigue que el caballero rampante"
        mensaje2 = "coja todas las llaves para abrir la puerta."
        mensaje3 = "Cuidado, si toca un pincho se le cae una llave..."
        mensaje4 = "(Pulsa enter para empezar)"
        fuente0 = os.path.join('fuentes', 'titulo.ttf')
        fuente = os.path.join('fuentes', 'mensajes.ttf')
        surf0 = utils.font2surf(mensaje0,75,color0,fuente0)
        surf1 = utils.font2surf(mensaje1,20,color,fuente)
        surf2 = utils.font2surf(mensaje2,20,color,fuente)
        surf3 = utils.font2surf(mensaje3,20,color,fuente)
        surf4 = utils.font2surf(mensaje4,10,color,fuente)
        self.screen.blit(surf0,(20,100))
        self.screen.blit(surf1,(50,270))
        self.screen.blit(surf2,(50,305))
        self.screen.blit(surf3,(50,340))
        self.screen.blit(surf4,(50,375))
        self.snd_intro.play(-1)     #loop infinito

    def inicio(self, exito = False):
        if exito:
            self.nivel+=1
            self.snd_victoria.play()
            self.snd_aplauso.play()
            self.snd_intro.play(-1)     #loop infinito
        else:
            self.snd_inicio.play()
        self.jugar = False
        self.esperando_intro = False
        self.esperando_creditos = False
        self.esperando_reinicio = False
        self.esperando_jugar = True

        self.tiempo = self.nivel+10
        if self.tiempo > 60:
            self.tiempo = 60

        self.llaves = pygame.sprite.RenderPlain()
        self.maxllaves_nivel = min(self.nivel + 2, self.maxllaves) #controlar el máximo de llaves
        for i in range(self.maxllaves_nivel):
            llavei = llave.Llave(self)
            self.llaves.add(llavei)

        self.meta = meta.Meta(self)
        self.metas = pygame.sprite.RenderPlain((self.meta))

        self.pinchos = pygame.sprite.RenderPlain()
        for i in range(self.nivel):
            pinchoi = pincho.Pincho(self)
            self.pinchos.add(pinchoi)
        if self.nivel > 9:
            pinchoi = pincho.Pincho(self,True)
            self.pinchos.add(pinchoi)

        self.prota = prota.Prota(self)
        self.protas = pygame.sprite.RenderPlain((self.prota))

        self.fondo.blit(self.imagen_fondo, (0,0))
        self.screen.blit(self.fondo, (0,0))
        pygame.display.flip()

        color=(255,240,0)
        mensaje1 = "A por las llaves... nivel " + str(self.nivel)
        mensaje2 = "(Pulsa enter)"
        fuente = os.path.join('fuentes', 'mensajes.ttf')
        surf1 = utils.font2surf(mensaje1,36,color,fuente)
        surf2 = utils.font2surf(mensaje2,10,color,fuente)
        self.screen.blit(surf1,(70,200))
        self.screen.blit(surf2,(70,250))
        self.esperando_jugar = True

    def empezar(self):
        self.esperando_jugar = False
        self.fondo.blit(self.imagen_fondo, (0,0))
        self.txt_nivel("Nivel " + str(self.nivel))
        self.screen.blit(self.fondo, (0,0))
        pygame.display.flip()
        pygame.time.set_timer(utils.CUENTA_ATRAS, 1000)  #para actualizar la cuenta atrás
        self.actualizar_cuenta_atras()
        self.meta.cerrar()
        self.jugar = True

    def comprueba_pinchos(self):
        colisiones = pygame.sprite.spritecollide(self.prota, self.pinchos, False)
        if len (colisiones) > 0:  #si toca algún pincho
            self.prota.pinchar()
            if len(self.llaves) < self.maxllaves_nivel: #controlar el máximo de llaves
                llavei = llave.Llave(self)
                self.llaves.add(llavei) #añadir una llave
                if self.puerta_abierta:
                    self.meta.cerrar()

if __name__ == "__main__":
    try:    # usar psyco si se puede
        import psyco
        psyco.full()
        print "Usando Psyco"
    except ImportError:
        pass

    base = Rampante()
