# coding: utf-8

#    El Caballero Rampante


import pygame, random
from pygame.locals import *
import rampante,utils

class Llave(pygame.sprite.Sprite):
    def __init__(self,bas):
        pygame.sprite.Sprite.__init__(self) #call Sprite intializer
        self.bas = bas
        self.image, self.rect = utils.load_image('llave.png', -1)
        self.area = self.bas.screen.get_rect()
        self.rect.topleft = 100 + random.random()*600, 100 + random.random()*400
        self.movex = 3 + random.random()*12
        self.movey = 3 + random.random()*12
        self.cogida = False

    def update(self):
        if not self.cogida:
            self.mover()

    def mover(self):
        newpos = self.rect.move((self.movex, self.movey))
        if self.rect.left < self.area.left or \
            self.rect.right > self.area.right:
            self.movex = -self.movex
            self.image = pygame.transform.flip(self.image, 1, 0)
        if self.rect.top < self.area.top or \
            self.rect.bottom > self.area.bottom:
            self.movey = -self.movey
        newpos = self.rect.move((self.movex, self.movey))
        self.rect = newpos

    def coger(self):
        self.cogida = True
        self.bas.llaves.clear(self.bas.screen,self.bas.fondo)
        self.bas.llaves.remove(self)
