# coding: utf-8

#    El Caballero Rampante

import pygame, os
from pygame.locals import *

#constantes, no interferir con las del sistema
CUENTA_ATRAS = USEREVENT
RECUPERAR_PROTA = USEREVENT + 1

#funciones
def load_image(name, colorkey=None):
    fullname = os.path.join('imagenes', name)
    try:
        image = pygame.image.load(fullname)
    except pygame.error, message:
        print "No se pudo cargar imagen:", name
        raise SystemExit, message
    image = image.convert()
    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0,0))
        image.set_colorkey(colorkey, RLEACCEL)
    return image, image.get_rect()

def load_sound(name):
    class NoneSound:
        def play(self): pass
    if not pygame.mixer:
        return NoneSound()
    fullname = os.path.join('sonidos', name)
    try:
        sound = pygame.mixer.Sound(fullname)
    except pygame.error, message:
        print "No se pudo cargar sonido:", fullname
        print message
        return NoneSound()
    sound.set_volume(0.5)
    return sound

def font2surf(word,size,fcol=None,ttf=None,fondo=None):
    if not fcol:
        fcol = (255,255,255)
    try:
        font = pygame.font.Font(ttf,size)
    except Exception,info:
        print >> sys.stderr,info,'Usando fuente estandar de pygame...'
        font = pygame.font.Font(None,size)
    if fondo is not None:
        surf = font.render(word, 1, fcol, fondo)
    else:
        surf = font.render(word, 1, fcol)
    return surf
